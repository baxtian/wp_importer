# DESCRIPTION

Class to be inherited to create a WP importer.

## Mantainers

Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>

## Changelog

# 0.3.25

* Solve bug that duplicates posts with an '&' in the title. 

# 0.3.24

* Use titles in p2p relationships

# 0.3.23

* Decode URL for titles in attachments

# 0.3.20

* Add public functions to import and export.

# 0.3.19

* Add type thumbnail.

# 0.3.18

* Add action MrkImporter/row_added.

# 0.3.17

* Import images.

# 0.3.16

* Import attachments.

# 0.3.15

* Allow to import p2p relationships.

# 0.3.14

* Allow import files in wp_content/uploads

# 0.3.13

* Options for media and attachment importer

# 0.3.12

* Timber 2.

# 0.3.10

* Resolve some bugs.

# 0.3.9

* Use direct filename for rendering.

# 0.3.8

* Use loader/paths filter only during rendering.

# 0.3.7

* Upgrade render to allow multiple merak instances 

# 0.3.6

* Fixed some bugs 

# 0.3.5

* Link styles 

# 0.3.4

* Add link to import anther file 

# 0.3.3

* Include option to save a copy of a filed into a postmeta 

# 0.3.2

* Add jquery-ui if it hasn't been declared 

# 0.3.1

* In case of using this class in multiple providers, allow Composer to set which file to use by default.

# 0.3.0

* Add testing.

# 0.2.1

* Refactoring.

# 0.1.14

* Import calculated cell, not formula.
* Add option to select export type in the import report.

# 0.1.13

* Fixing bugs

# 0.1.12

* Allow to use PHP8.0

## 0.1.11

* Check ID first.

## 0.1.10

* Improves, fixes and documentation.

## 0.1.9

* Add more width space to export dialog.

## 0.1.8

* Import and export excel files.

## 0.1.7

* Detect codification and Convert to UTF-8 if required.
* Include postmeta_date to type importer.
* Include postmeta_datetime to type importer.

## 0.1.6

* Manage empty terms on import.

## 0.1.5

* Include postmeta_textarea to type importer.

## 0.1.4

* Bug: importer generate errors when CSV have string with line breaks.

## 0.1.3

* Bug: templates directory not working on some servers.

## 0.1.2

* Update translations
* Bug: Always importing tha same post type.

## 0.1.1

* Bug: WP_Importer has been in use by WP.

## 0.1

* First stable release
