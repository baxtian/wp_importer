<?php
namespace Tests;

use Mockery\Adapter\Phpunit\MockeryTestCase;
use Brain\Monkey;

class MonkeyTestCase extends MockeryTestCase
{
	protected function setUp():void
	{
		parent::setUp();
		Monkey\setUp();
	}

	protected function tearDown():void
	{
		Monkey\tearDown();
		parent::tearDown();
	}
}
