<?php
namespace Tests;

// Entorno de testeto
use Tests\MonkeyTestCase;
use Brain\Monkey;
use Mockery;
use Exception;

// Clases y dependencias a probar
use Baxtian\WP_Importer\Data\StructureImporter;
use Baxtian\WP_Importer\Files\ImportCSV;
use Baxtian\WP_Importer\Data\ImportBatch;

class DataImportBatchTest extends MonkeyTestCase
{
	protected function setUp(): void
	{
		parent::setUp();
		Monkey\Functions\when('__')
			->returnArg(1);
	}

	public function testReadingFile()
	{
		// Simular el importador de estructuras
		$structure = Mockery::mock(StructureImporter::class);
		$structure->shouldReceive('importData')
			->andReturnUsing(function () {
				return [];
			});

		// Simular el importador de estructuras
		$all_data = [
			0 => [
				'titulo', 'id', 'texto',
			],
			1 => [
				'a', 1, 'aaa',
			],
			2 => [
				'b', 2, 'bbb',
			],
			3 => [
				'c', 3, 'ccc',
			],
		];

		// Arreglo de 3 items
		// Importar primer lote [0 a 1] y con siguiente lote [2]
		$sut    = new ImportBatch();
		$answer = $sut->import($all_data, $structure, 1, 2);
		$this->assertEquals($answer['primero'], 0);
		$this->assertEquals($answer['ultimo'], 1);
		$this->assertEquals($answer['siguiente'], 2);

		// Importar segundo lote [2 a 3] y sin siguiente lote
		$answer = $sut->import($all_data, $structure, 2, 2);

		$this->assertEquals($answer['primero'], 2);
		$this->assertEquals($answer['ultimo'], 3);
		$this->assertEquals($answer['siguiente'], 0);
	}
}
