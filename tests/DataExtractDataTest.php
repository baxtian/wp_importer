<?php
namespace Tests;

// Entorno de testeto
use Tests\MonkeyTestCase;
use Brain\Monkey;
use Mockery;
use Exception;

// Clases y dependencias a probar
use Baxtian\WP_Importer\Data\ExtractData;
use Baxtian\WP_Importer\Files\ExportCSV;

class DataExtractDataTest extends MonkeyTestCase
{
	protected $campos;

	protected function setUp(): void
	{
		parent::setUp();
		Monkey\Functions\when('__')
			->returnArg(1);

		Monkey\Functions\when('wp_die')
			->justReturn(true);

		$this->campos = [
			[
				'name' => 'id',
				'type' => 'id',
			],
			[
				'name' => 'post_title',
				'type' => 'field',
			],
			[
				'name' => 'subtitle',
				'type' => 'postmeta',
			],
			[
				'name' => 'post_content',
				'type' => 'field',
			],
			[
				'name' => 'category',
				'type' => 'term',
			],
			// [
			// 	'name' => 'post_term',
			// 	'type' => 'term',
			// ],
			// [
			// 	'name'        => 'company',
			// 	'type'        => 'term',
			// 	'entire_text' => true,

			// ],
			// [
			// 	'name' => 'textarea',
			// 	'type' => 'postmeta_textarea',
			// ],
			// [
			// 	'name' => 'date',
			// 	'type' => 'postmeta_date',
			// ],
			// [
			// 	'name' => 'datetime',
			// 	'type' => 'postmeta_datetime',
			// ],
			[
				'name' => 'bool',
				'type' => 'postmeta_bool',
			],
		];
	}

	public function testExportData()
	{
		$post1               = Mockery::mock('post');
		$post1->id           = 101;
		$post1->post_title   = 'Title 1';
		$post1->subtitle     = 'Subtitle 1';
		$post1->post_content = 'Content 1';
		$post1->bool         = false;
		$post1->shouldReceive('terms')
			->with(
				\Mockery::any(),
			)
			->andReturnUsing(function () {
				return [
					(object) [
						'name' => 'Cat 1',
					],
				];
			});

		$post2               = Mockery::mock('post');
		$post2->id           = 102;
		$post2->post_title   = 'Title 2';
		$post2->subtitle     = 'Subtitle 2';
		$post2->post_content = 'Content 2';
		$post2->bool         = false;
		$post2->shouldReceive('terms')
				->with(
					\Mockery::any(),
				)
				->andReturnUsing(function () {
					return [
						(object) [
							'name' => 'Cat 2',
						],
					];
				});

		$post3               = Mockery::mock('post');
		$post3->id           = 103;
		$post3->post_title   = 'Title 3';
		$post3->subtitle     = 'Subtitle 3';
		$post3->post_content = 'Content 3';
		$post3->bool         = true;
		$post3->shouldReceive('terms')
			->with(
				\Mockery::any(),
			)
			->andReturnUsing(function () {
				return [
					(object) [
						'name' => 'Cat 1',
					],
					(object) [
						'name' => 'Cat 3',
					],
				];
			});

		Mockery::mock('alias:Timber\Timber')
			->shouldReceive('get_posts')
			->andReturn([
				$post1,
				$post2,
				$post3,
			]);

		$sut  = new ExtractData();
		$data = $sut->export('post', 'posts', $this->campos);
		$this->assertEquals($data[0][0], 'id');
		$this->assertEquals($data[1][0], '101');
		$this->assertEquals($data[2][1], 'Title 2');
		$this->assertEquals($data[3][2], 'Subtitle 3');
		$this->assertEquals($data[2][3], 'Content 2');
		$this->assertEquals($data[3][4], 'Cat 1, Cat 3');
		$this->assertEquals($data[2][5], 0);
		$this->assertEquals($data[3][5], 1);
	}

	public function testExportDataWithoutItems()
	{
		Mockery::mock('alias:Timber\Timber')
			->shouldReceive('get_posts')
			->andReturn([]);

		$sut  = new ExtractData();
		$data = $sut->export('post', 'posts', $this->campos);
		$this->assertEquals($data[0][0], 'post_title');
		$this->assertEquals($data[0][1], 'subtitle');
		$this->assertEquals($data[0][2], 'post_content');
	}
}
