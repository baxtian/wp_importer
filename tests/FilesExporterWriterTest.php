<?php
namespace Tests;

// Entorno de testeto
use Tests\MonkeyTestCase;
use Brain\Monkey;
use Mockery;
use Exception;

// Clases y dependencias a probar
use Baxtian\WP_Importer\Files\ExporterWriter;
use Baxtian\WP_Importer\Files\ExportCSV;
use Baxtian\WP_Importer\Files\ExportXLSX;

class FilesExporterWriterTest extends MonkeyTestCase
{
	protected function setUp(): void
	{
		parent::setUp();
		Monkey\Functions\when('__')
			->returnArg(1);
	}

	public function testUnrecognizedType()
	{
		$this->expectException(Exception::class);

		$sut = new ExporterWriter();
		$sut->getExporter('txt');
	}

	public function testGetCsvExporter()
	{
		$sut    = new ExporterWriter();
		$writer = $sut->getExporter('csv');
		$this->assertTrue($writer instanceof ExportCSV);
	}

	public function testGetXlsxExporter()
	{
		$sut    = new ExporterWriter();
		$writer = $sut->getExporter('xlsx');
		$this->assertTrue($writer instanceof ExportXLSX);
	}
}
