<?php
namespace Tests;

// Entorno de testeto
use Tests\MonkeyTestCase;
use Brain\Monkey;
use Mockery;
use Exception;

// Clases y dependencias a probar
use \PhpOffice\PhpSpreadsheet\Spreadsheet;
use \PhpOffice\PhpSpreadsheet\IOFactory;
use \PhpOffice\PhpSpreadsheet\Reader\Exception as XEception;
use Baxtian\WP_Importer\Files\ImportXLSX;

class FilesImportXLSXTest extends MonkeyTestCase
{
	protected function setUp(): void
	{
		parent::setUp();
		Monkey\Functions\when('__')
			->returnArg(1);
	}

	public function testErrorReadingWithoutFile()
	{
		$this->expectException(Exception::class);

		$sut = new ImportXLSX();
		$sut->fileToData();
	}

	public function testErrorReadingFile()
	{
		$this->expectException(Exception::class);

		$sut = new ImportXLSX();
		$sut->setFile('archivo');
		$sut->fileToData();
	}

	public function testReadingFile()
	{
		$sut = new ImportXLSX();
		$sut->setFile(__DIR__ . '/assets/spreadsheet.xlsx');
		$data = $sut->fileToData();
		$this->assertEquals($data[0][0], 'a1');
		$this->assertEquals($data[1][2], 'c2');
	}

	public function testDeleteFile()
	{
		$temp = tempnam(sys_get_temp_dir(), 'TMP_');
		file_put_contents($temp, file_get_contents(__DIR__ . '/assets/spreadsheet.xlsx'));

		$sut = new ImportXLSX();
		$sut->setFile($temp);

		$this->fileExists($temp);
		$sut->deleteFile();
		$this->assertFalse(file_exists($temp));
	}
}
