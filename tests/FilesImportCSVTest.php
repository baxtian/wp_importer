<?php
namespace Tests;

// Entorno de testeto
use Tests\MonkeyTestCase;
use Brain\Monkey;
use Mockery;
use Exception;

// Clases y dependencias a probar
use Baxtian\WP_Importer\Files\ImportCSV;

class FilesImportCSVTest extends MonkeyTestCase
{
	protected function setUp(): void
	{
		parent::setUp();
		Monkey\Functions\when('__')
			->returnArg(1);
	}

	public function testErrorReadingWithoutFile()
	{
		$this->expectException(Exception::class);

		$sut = new ImportCSV();
		$sut->fileToData();
	}

	public function testErrorReadingFile()
	{
		$this->expectException(Exception::class);

		$sut = new ImportCSV();
		$sut->setFile(__DIR__ . '/assets/no_file.csv');
		$sut->fileToData();
	}

	public function testReadingFile()
	{
		$sut = new ImportCSV();
		$sut->setFile(__DIR__ . '/assets/spreadsheet.csv');
		$data = $sut->fileToData();
		$this->assertEquals($data[0][0], 'a1');
		$this->assertEquals($data[1][2], 'c2');
	}

	public function testReading_No_UTF8_File()
	{
		$sut = new ImportCSV();
		$sut->setFile(__DIR__ . '/assets/spreadsheet_noutf8.csv');
		$data = $sut->fileToData();
		$this->assertEquals($data[2][0], 'á');
		$this->assertEquals($data[2][1], 'é');
	}

	public function testDeleteFile()
	{
		$temp = tempnam(sys_get_temp_dir(), 'TMP_');
		file_put_contents($temp, file_get_contents(__DIR__ . '/assets/spreadsheet.csv'));

		$sut = new ImportCSV();
		$sut->setFile($temp);

		$this->fileExists($temp);
		$sut->deleteFile();
		$this->assertFalse(file_exists($temp));
	}
}
