<?php
namespace Tests;

// Entorno de testeto
use Tests\MonkeyTestCase;
use Brain\Monkey;
use Mockery;
use Exception;

// Clases y dependencias a probar
use Baxtian\WP_Importer\Files\ImporterReader;
use Baxtian\WP_Importer\Files\ImportCSV;
use Baxtian\WP_Importer\Files\ImportXLSX;

class FilesImporterReaderTest extends MonkeyTestCase
{
	protected function setUp(): void
	{
		parent::setUp();
		Monkey\Functions\when('__')
			->returnArg(1);
	}

	public function testUnrecognizedType()
	{
		$this->expectException(Exception::class);

		$sut = new ImporterReader();
		$sut->getImporter('txt');
	}

	public function testGetCsvImporter()
	{
		$sut    = new ImporterReader();
		$reader = $sut->getImporter('csv');
		$this->assertTrue($reader instanceof ImportCSV);
	}

	public function testGetXlsxImporter()
	{
		$sut    = new ImporterReader();
		$reader = $sut->getImporter('xlsx');
		$this->assertTrue($reader instanceof ImportXLSX);
	}
}
