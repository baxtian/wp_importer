<?php

namespace Tests;

// Entorno de testeto
use Tests\MonkeyTestCase;
use Brain\Monkey;
use Mockery;
use Exception;

// Clases y dependencias a probar
use Baxtian\WP_Importer\Data\StructureImporter;

class DataStructureImporterTest extends MonkeyTestCase
{
	protected $importer;
	protected $data;

	protected function setUp(): void
	{
		global $term_cat1;

		$term_cat1 = false;
		parent::setUp();
		Monkey\Functions\when('__')
			->returnArg(1);
		Monkey\Functions\when('sanitize_text_field')
			->returnArg(1);
		Monkey\Functions\when('wp_set_post_terms')
			->justReturn(true);
		Monkey\Functions\when('wp_update_post')
			->justReturn(true);
		Monkey\Functions\when('get_term_by')
			->alias(function ($name, $item, $taxonomia) {
				$term = Mockery::mock('WP_Term');
				if ($item == 'Cat 1') {
					global $term_cat1;

					if ($term_cat1) {
						$term->term_id = $term_cat1;
						$term->name    = $item;
					} else {
						$term_cat1 = 11;
						$term      = false;
					}
				}
				if ($item == 'Cat 2') {
					$term->term_id = 12;
					$term->name    = $item;
				}
				if ($item == 'Term 1') {
					$term->term_id = 21;
					$term->name    = $item;
				}
				if ($item == 'Term 2') {
					$term->term_id = 22;
					$term->name    = $item;
				}
				if ($item == 'Term A') {
					$term->term_id = 31;
					$term->name    = $item;
				}
				if ($item == 'Term B') {
					$term->term_id = 32;
					$term->name    = $item;
				}
				if ($item == '1, 2, 3') {
					$term->term_id = 41;
					$term->name    = $item;
				}
				if ($item == '2, 3, 4, 5') {
					$term->term_id = 42;
					$term->name    = $item;
				}

				return $term;
			});
		Monkey\Functions\when('wp_insert_term')
			->alias(function ($item, $taxonomia) {
				$term = false;
				if ($item == 'Cat 1') {
					$term = 11;
				}
				if ($item == 'Cat 2') {
					$term = 12;
				}
				if ($item == 'Term 1') {
					$term = 21;
				}
				if ($item == 'Term 2') {
					$term = 22;
				}
				if ($item == 'Term A') {
					$term = 31;
				}
				if ($item == 'Term B') {
					$term = 32;
				}
				if ($item == '1, 2, 3') {
					$term = 41;
				}
				if ($item == '2, 3, 4, 5') {
					$term = 42;
				}

				return $term = [
					'term_id' => $term,
				];
			});
		Monkey\Functions\when('wp_insert_post')
			->alias(function ($args) {
				if ($args['post_title'] == 'Title 1') {
					return 1;
				}
				if ($args['post_title'] == 'Title 2') {
					return 2;
				}
				if ($args['post_title'] == 'Title 3') {
					return 3;
				}

				return false;
			});
		Monkey\Functions\when('wp_kses')
			->alias(function ($value, $allowed_html) {
				return $value;
			});

		$this->importer = (object) [
			'tipo'   => 'post',
			'tipos'  => 'posts',
			'campos' => [
				[
					'name' => 'id',
					'type' => 'id',
				],
				[
					'name'     => 'post_title',
					'type'     => 'field',
					'postmeta' => 'name',
				],
				[
					'name' => 'subtitle',
					'type' => 'postmeta',
				],
				[
					'name' => 'post_content',
					'type' => 'field',
				],
				[
					'name' => 'category',
					'type' => 'term',
				],
				[
					'name' => 'post_term',
					'type' => 'term',
				],
				[
					'name'        => 'company',
					'type'        => 'term',
					'entire_text' => true,

				],
				[
					'name' => 'textarea',
					'type' => 'postmeta_textarea',
				],
				[
					'name' => 'date',
					'type' => 'postmeta_date',
				],
				[
					'name' => 'datetime',
					'type' => 'postmeta_datetime',
				],
				[
					'name' => 'bool',
					'type' => 'postmeta_bool',
				],
			],
		];
		$this->data = [
			[
				'id'           => 1,
				'post_title'   => 'Title A',
				'subtitle'     => 'Subtitle A',
				'post_content' => 'Contant A',
				'category'     => 'Cat 1',
				'textarea'     => 'Textarea A',
				'date'         => '2000-01-01',
				'datetime'     => '2000-01-01 12:00',
				'bool'         => true,
				'post_term'    => 'Term 1, Term A',
				'company'      => '1, 2, 3',
			],
			[
				'id'           => 2,
				'post_title'   => 'Title B',
				'subtitle'     => 'Subtitle B',
				'post_content' => 'Contant B',
				'category'     => 'Cat 1',
				'textarea'     => 'Textarea B',
				'date'         => '2001-01-01',
				'datetime'     => '2001-01-01 12:00',
				'bool'         => false,
				'post_term'    => 'Term 1, Term B',
				'company'      => '1, 2, 3',
			],
			[
				'id'           => 3,
				'post_title'   => 'Title C',
				'subtitle'     => 'Subtitle C',
				'post_content' => 'Contant C',
				'category'     => 'Cat 2',
				'textarea'     => 'Textarea C',
				'date'         => '2002-01-01',
				'datetime'     => '2002-01-01 12:00',
				'bool'         => true,
				'post_term'    => 'Term 2, Term B',
				'company'      => '2, 3, 4, 5',
			],
		];
	}

	public function testImportWithoutId()
	{
		$this->data[0]['id'] = '';
		$this->data[1]['id'] = '';
		$this->data[2]['id'] = '';

		// Mock wpdb
		global $wpdb;
		$wpdb = Mockery::mock('wpdb');

		$wpdb->prefix = 'wpimp_';
		$wpdb->posts  = 'posts';
		$wpdb
			->shouldReceive('prepare')
			->with(
				\Mockery::any(),
				\Mockery::anyOf('Title A', 'Title B', 'Title C')
			)
			->andReturnUsing(function ($sql, $title) {
				if ($title == 'Title A') {
					return 1;
				}
				if ($title == 'Title B') {
					return 2;
				}
				if ($title == 'Title C') {
					return 3;
				}

				return false;
			});

		$wpdb
			->shouldReceive('get_var')
			->with(\Mockery::anyOf(1, 2, 3))
			->andReturnUsing(function ($id) {
				return $id;
			});

		// Función update_post_meta
		Monkey\Functions\when('update_post_meta')
			->justReturn(true);

		// Test
		// Agregar 3 items que ya tiene ID (modificar)
		// Debe crearse dos categorías nuevas
		$sut         = new StructureImporter();
		$sut->tipo   = $this->importer->tipo;
		$sut->campos = $this->importer->campos;
		$sut->importData($this->data);
		$acciones = $sut->acciones;

		$this->assertEquals($sut->tipo, $this->importer->tipo);
		$this->assertEquals($sut->campos, $this->importer->campos);
		$this->assertEquals($acciones['nuevas'], 0);
		$this->assertEquals($acciones['modificadas'], 3);
	}

	public function testImportWithId()
	{
		// Mock wpdb
		global $wpdb;
		$wpdb = Mockery::mock('wpdb');

		$wpdb->prefix = 'wpimp_';
		$wpdb->posts  = 'posts';
		$wpdb
			->shouldReceive('prepare')
			->with(
				\Mockery::any(),
				\Mockery::anyOf(1, 2, 3)
			)
			->andReturnUsing(function ($sql, $id) {
				return $id;
			});

		$wpdb
			->shouldReceive('get_var')
			->with(\Mockery::anyOf(1, 2, 3))
			->andReturnUsing(function ($id) {
				return $id;
			});

		// Función update_post_meta
		Monkey\Functions\when('update_post_meta')
			->justReturn(true);

		// Test
		// Agregar 3 items que ya tiene ID (modificar)
		// Debe crearse dos categorías nuevas
		$sut         = new StructureImporter();
		$sut->tipo   = $this->importer->tipo;
		$sut->campos = $this->importer->campos;

		$sut->importData($this->data);
		$acciones = $sut->acciones;

		$this->assertEquals($acciones['nuevas'], 0);
		$this->assertEquals($acciones['modificadas'], 3);
	}

	public function testImportNewPosts()
	{
		Monkey\Functions\when('update_post_meta')
			->justReturn(true);

		$this->data[0]['id'] = '';
		$this->data[1]['id'] = '';
		$this->data[2]['id'] = '';

		// Mock wpdb
		global $wpdb;
		$wpdb = Mockery::mock('wpdb');

		$wpdb->prefix = 'wpimp_';
		$wpdb->posts  = 'posts';
		$wpdb
			->shouldReceive('prepare')
			->with(
				\Mockery::any(),
				\Mockery::anyOf('Title A', 'Title B', 'Title C')
			)
			->andReturnUsing(function ($sql, $title) {
				if ($title == 'Title A') {
					return 1;
				}
				if ($title == 'Title B') {
					return 2;
				}
				if ($title == 'Title C') {
					return 3;
				}

				return false;
			});

		$wpdb
			->shouldReceive('get_var')
			->with(\Mockery::anyOf(1, 2, 3))
			->andReturnUsing(function ($id) {
				return false;
			});

		// Test
		// Agregar 3 items que ya tiene ID (modificar)
		// Debe crearse dos categorías nuevas
		$sut         = new StructureImporter();
		$sut->tipo   = $this->importer->tipo;
		$sut->campos = $this->importer->campos;

		$sut->importData($this->data);
		$acciones = $sut->acciones;

		$this->assertEquals($acciones['nuevas'], 3);
		$this->assertEquals($acciones['modificadas'], 0);
	}

	public function testTermManagment()
	{
		// Mock wpdb
		global $wpdb;
		$wpdb = Mockery::mock('wpdb');

		$wpdb->prefix = 'wpimp_';
		$wpdb->posts  = 'posts';
		$wpdb
			->shouldReceive('prepare')
			->with(
				\Mockery::any(),
				\Mockery::anyOf(1, 2, 3)
			)
			->andReturnUsing(function ($sql, $id) {
				return $id;
			});

		$wpdb
			->shouldReceive('get_var')
			->with(\Mockery::anyOf(1, 2, 3))
			->andReturnUsing(function ($id) {
				return $id;
			});

		// Función update_post_meta
		Monkey\Functions\when('update_post_meta')
			->justReturn(true);

		// Test
		// Agregar 3 items que ya tiene ID (modificar)
		// Debe crearse dos categorías nuevas
		$sut         = new StructureImporter();
		$sut->tipo   = $this->importer->tipo;
		$sut->campos = $this->importer->campos;

		$sut->importData($this->data);
		$taxonomias = $sut->taxonomias;
		$acciones   = $sut->acciones;

		$this->assertEquals($taxonomias['category']['Cat 1'], 11);
		$this->assertEquals($taxonomias['category']['Cat 2'], 12);
		$this->assertEquals($taxonomias['post_term']['Term 1'], 21);
		$this->assertEquals($taxonomias['post_term']['Term 2'], 22);
		$this->assertEquals($taxonomias['post_term']['Term A'], 31);
		$this->assertEquals($taxonomias['post_term']['Term B'], 32);
		$this->assertEquals($taxonomias['company']['1, 2, 3'], 41);
		$this->assertEquals($taxonomias['company']['2, 3, 4, 5'], 42);
		$this->assertEquals($acciones['terminos_nuevos'], 1);
	}

	public function testImportPostmeta()
	{
		// Mock wpdb
		global $wpdb;
		$wpdb = Mockery::mock('wpdb');

		$wpdb->prefix = 'wpimp_';
		$wpdb->posts  = 'posts';
		$wpdb
			->shouldReceive('prepare')
			->with(
				\Mockery::any(),
				\Mockery::anyOf(1, 2, 3)
			)
			->andReturnUsing(function ($sql, $id) {
				return $id;
			});

		$wpdb
			->shouldReceive('get_var')
			->with(\Mockery::anyOf(1, 2, 3))
			->andReturnUsing(function ($id) {
				return $id;
			});

		// Postmeta 'name'
		Monkey\Functions\expect('update_post_meta')
			->times(3)
			->with(
				Mockery::type('int'),
				Mockery::anyOf('name'),
				Mockery::anyOf('Title A', 'Title B', 'Title C')
			)
			->andReturn(true);

		// Postmeta 'subtitle'
		Monkey\Functions\expect('update_post_meta')
			->times(3)
			->with(
				Mockery::type('int'),
				Mockery::anyOf('subtitle'),
				Mockery::anyOf('Subtitle A', 'Subtitle B', 'Subtitle C')
			)
			->andReturn(true);

		// Postmeta 'textarea'
		Monkey\Functions\expect('update_post_meta')
			->times(3)
			->with(
				Mockery::type('int'),
				Mockery::anyOf('textarea'),
				Mockery::anyOf('Textarea A', 'Textarea B', 'Textarea C')
			)
			->andReturn(true);

		// Postmeta 'date'
		Monkey\Functions\expect('update_post_meta')
			->times(3)
			->with(
				Mockery::type('int'),
				Mockery::anyOf('date'),
				Mockery::anyOf('2000-01-01', '2001-01-01', '2002-01-01')
			)
			->andReturn(true);

		// Postmeta 'date'
		Monkey\Functions\expect('update_post_meta')
			->times(3)
			->with(
				Mockery::type('int'),
				Mockery::anyOf('datetime'),
				Mockery::anyOf('2000-01-01 12:00:00', '2001-01-01 12:00:00', '2002-01-01 12:00:00')
			)
			->andReturn(true);

		// Postmeta 'bool'
		Monkey\Functions\expect('update_post_meta')
			->times(3)
			->with(
				Mockery::type('int'),
				Mockery::anyOf('bool'),
				Mockery::anyOf(true, false, true)
			)
			->andReturn(true);

		// Test
		// Agregar 3 items que ya tiene ID (modificar)
		// Debe crearse dos categorías nuevas
		$sut         = new StructureImporter();
		$sut->tipo   = $this->importer->tipo;
		$sut->campos = $this->importer->campos;

		$sut->importData($this->data);
		$acciones = $sut->acciones;

		$this->assertEquals($acciones['nuevas'], 0);
		$this->assertEquals($acciones['modificadas'], 3);
	}
}
