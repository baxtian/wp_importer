<?php
namespace Tests;

// Entorno de testeto
use Tests\MonkeyTestCase;
use Brain\Monkey;
use Mockery;

// Clases y dependencias a probar
use Baxtian\WP_Importer;
use Baxtian\WP_Importer\Views\ImporterPage;
use Baxtian\WP_Importer\Views\Render;
use Baxtian\WP_Importer\Data\ImportBatch;
use Baxtian\WP_Importer\Data\ExtractData;
use Baxtian\WP_Importer\Data\StructureImporter;
use Baxtian\WP_Importer\Files\ExportCSV;
use Baxtian\WP_Importer\Files\ExportWriter;
use Baxtian\WP_Importer\Files\ImportCSV;
use Baxtian\WP_Importer\Files\ImportReader;
use Baxtian\WP_Importer\Files\ImportFileInterface;
use Exception;

class WpImporterTest extends MonkeyTestCase
{
	protected function setUp(): void
	{
		parent::setUp();
		Monkey\Functions\when('__')
			->returnArg(1);

		Monkey\Functions\when('sanitize_text_field')
			->returnArg(1);

		Monkey\Functions\when('wp_die')
			->justReturn(true);
	}

	public function testImportUnknownFiletype()
	{
		$_GET['step']       = 0;
		$_GET['filename']   = 'test';
		$_GET['filetype']   = 'txt';
		$_GET['p2p_action'] = StructureImporter::WITHOUT_P2P;

		$sut    = new WP_Importer();
		$answer = $sut->_import();
		$this->expectOutputString('{"error":true,"message":"Unrecognized type."}');
	}

	public function testImport()
	{
		$_GET['step']       = 1;
		$_GET['filename']   = __DIR__ . '/assets/posts.csv';
		$_GET['filetype']   = 'csv';
		$_GET['p2p_action'] = StructureImporter::WITHOUT_P2P;

		$importBatch = Mockery::mock(ImportBatch::class);
		$importBatch->shouldReceive('import')
			->andReturn([
				'siguiente' => 1, // Para no borrar el archivo de pruebas
				'acciones'  => [
					'nuevas'          => 1,
					'modificadas'     => 1,
					'terminos_nuevos' => 1,
				],
			]);

		$sut = new WP_Importer(['import_batch' => $importBatch]);

		$sut->tipo           = 'post';
		$sut->tipos          = 'posts';
		$sut->items_per_page = 50;
		$sut->campos         = [
			[
				'name' => 'id',
				'type' => 'id',
			],
			[
				'name' => 'post_title',
				'type' => 'field',
			],
			[
				'name' => 'subtitle',
				'type' => 'postmeta',
			],
			[
				'name' => 'post_content',
				'type' => 'field',
			],
			[
				'name' => 'category',
				'type' => 'term',
			],
		];
		$sut->textos = [
			'mensaje_publicacion' => 'message',
		];

		$sut->_import();

		$this->expectOutputString('{"siguiente":1,"acciones":{"nuevas":1,"modificadas":1,"terminos_nuevos":1},"mensaje":"message"}');
	}

	public function testImportXlsxWithError()
	{
		$_GET['step']       = 1;
		$_GET['filename']   = __DIR__ . '/assets/spreadsheet_error.xlsx';
		$_GET['filetype']   = 'xls';
		$_GET['p2p_action'] = StructureImporter::WITHOUT_P2P;

		$sut = new WP_Importer();

		$sut->tipo           = 'post';
		$sut->tipos          = 'posts';
		$sut->items_per_page = 50;

		$sut->_import();

		$this->expectOutputString('{"error":true,"message":"Error while reading XLSX file."}');
	}

	public function testExport()
	{
		$_GET['type'] = 'csv';

		$extractData = Mockery::mock(ExtractData::class);
		$extractData->shouldReceive('export')
			->andReturn([
				[
					0 => 'id',
					1 => 'post_title',
					2 => 'subtitle',
					3 => 'post_content',
					4 => 'category',
					5 => 'bool',
				],
				[
					0 => '101',
					1 => 'Title 1',
					2 => 'Subtitle 1',
					3 => 'Content 1',
					4 => 'Cat 1',
					5 => '0',
				],
				[
					0 => '102',
					1 => 'Title 2',
					2 => 'Subtitle 2',
					3 => 'Content 2',
					4 => 'Cat 2',
					5 => '0',
				],
				[
					0 => '103',
					1 => 'Title 3',
					2 => 'Subtitle 3',
					3 => 'Content 3',
					4 => 'Cat 1, Cat 3',
					5 => '1',
				], ]);

		$writer = Mockery::mock(ExportCSV::class);
		$writer
			->shouldReceive('dataToFile')
			->andReturn([]);

		$exporter_writer = Mockery::mock(ExportWriter::class);
		$writer
			->shouldReceive('getExporter')
			->andReturn($writer);

		$sut = new WP_Importer([
			'extract_data'    => $extractData,
			'exporter_writer' => $writer,
		]);
		$sut->tipo           = 'post';
		$sut->tipos          = 'posts';
		$sut->items_per_page = 50;
		$sut->campos         = [
			[
				'name' => 'id',
				'type' => 'id',
			],
			[
				'name' => 'post_title',
				'type' => 'field',
			],
			[
				'name' => 'subtitle',
				'type' => 'postmeta',
			],
			[
				'name' => 'post_content',
				'type' => 'field',
			],
			[
				'name' => 'category',
				'type' => 'term',
			],
		];
		$sut->textos = [
			'mensaje_publicacion' => 'message',
		];

		$sut->_export();
	}

	public function testExportUnknownType()
	{
		$_GET['type'] = 'txt';

		$sut = new WP_Importer();

		$sut->tipo           = 'post';
		$sut->tipos          = 'posts';
		$sut->items_per_page = 50;

		$sut->_export();

		$this->expectOutputString('{"error":true,"message":"Unrecognized type."}');
	}

	public function testExportCsvWithException()
	{
		$_GET['type'] = 'csv';

		$extractData = Mockery::mock(ExtractData::class);
		$extractData->shouldReceive('export')
			->andReturn([]);

		$sut = new WP_Importer([
			'extract_data' => $extractData,
		]);

		$sut->tipo           = 'post';
		$sut->tipos          = 'posts';
		$sut->items_per_page = 50;

		$sut->_export();

		$this->expectOutputString('{"error":true,"message":"Error while exporting CSV file."}');
	}

	public function testExportXlsxWithException()
	{
		$_GET['type'] = 'xls';

		$extractData = Mockery::mock(ExtractData::class);
		$extractData->shouldReceive('export')
			->andReturn([]);

		$sut = new WP_Importer([
			'extract_data' => $extractData,
		]);

		$sut->tipo           = 'post';
		$sut->tipos          = 'posts';
		$sut->items_per_page = 50;

		$sut->_export();

		$this->expectOutputString('{"error":true,"message":"Error while exporting XLSX file."}');
	}

	public function testSetAndGet()
	{
		$sut        = new WP_Importer();
		$sut->tipo  = 'post';
		$sut->tipos = 'posts';

		$this->assertEquals($sut->tipo, 'post');
		$this->assertEquals($sut->tipos, 'posts');
		$this->assertFalse($sut->error);
	}

	public function testImporterPage()
	{
		// Simular importer_page
		$importer_page = Mockery::mock(ImporterPage::class);
		$importer_page->shouldReceive('getView')
			->andReturn([
				'tpl',
				['arg1' => 1, 'arg2' => 2],
			]);

		// Simular render
		$render = Mockery::mock(Render::class);
		$render->shouldReceive('render')
			->times(1)
			->andReturn('render');

		$sut = new WP_Importer([
			'importer_page' => $importer_page,
			'render'        => $render,
		]);
		$sut->tipo   = 'structure';
		$sut->tipos  = 'structures';
		$sut->textos = [];
		$sut->campos = [];
		$sut->accion = 'accion';
		$data        = $sut->page();
	}
}
