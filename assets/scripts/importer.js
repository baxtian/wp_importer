function importer(action, step, filename, filetype, p2p_action) {
	//Lista de acciones
	$acciones = jQuery('.importador .acciones');

	//Agregar acción actual
	$acciones.append('<li>' + importer_var.step + ' ' + step + '</li>');

	//Llamar a la acción
	jQuery.get(ajaxurl, {
		action: action,
		step: step,
		filename: filename,
		filetype: filetype,
		p2p_action: p2p_action
	}).done(function (obj) {
		var data = jQuery.parseJSON(obj);
		if (data.error === undefined) {
			//Agregar mensaje al último li que tengamos en las acciones
			var text = data.mensaje.split(",").join("</li><li>");
			$acciones.children('li:last-child').append("<ul><li>" + text + "</li></ul>");

			//Sumar a los totales
			jQuery.each(data.acciones, function (index, value) {
				var val = jQuery('#' + index).html();
				jQuery('#' + index).html((val * 1) + value);
			});

			//Si es el último
			if (data.siguiente == 0) {
				//Indicar en lista de acciones
				$acciones.append('<li>' + importer_var.finished + '</li>');

				//Indicar que hemos finalizado
				jQuery('.importador').removeClass('loading');

			} else { //Si no es el último,
				//Llamar recursivamente con nuevo paso
				importer(action, data.siguiente, filename, filetype, p2p_action);
			}

		} else {
			// Indicar el error
			jQuery('.importador .totales').html("<h2>" + data.message + "</h2>");
			jQuery('.importador p.finished').remove();

			//Indicar que hemos finalizado
			jQuery('.importador').removeClass('loading');

		}
	});
}

jQuery(document).ready(function ($) {
	jQuery(".wp_importer .export_link").on('click', function () {
		var url = jQuery('.importador .dialogo').attr('data-enlace');
		var title = jQuery('.importador .dialogo').attr('data-title');
		var cancel = jQuery('.importador .dialogo').attr('data-cancel');
		jQuery('.importador .dialogo p').dialog({
			title: title,
			modal: true,
			width: 500,
			close: function (event, ui) {
				jQuery(this).dialog("destroy");
			 },
			buttons: [
				{
					text: "CSV",
					click: function() {
						url = url + "&type=csv",
						window.open(url, "_self");
						jQuery(this).dialog("destroy");
					}
				},
				{
					text: "XLSX",
					click: function() {
						url = url + "&type=xlsx",
						window.open(url, "_self");
						jQuery(this).dialog("destroy");
					}
				},
				{
					text: cancel,
					click: function() {
						jQuery(this).dialog("destroy");
					}
				},
			]
		});
	});
});
