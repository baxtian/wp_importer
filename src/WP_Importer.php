<?php
namespace Baxtian;

use Baxtian\WP_Importer\Data\Attachments;
use Baxtian\WP_Importer\Views\ImporterPage;
use Baxtian\WP_Importer\Views\Render;

use Baxtian\WP_Importer\Files\ExporterWriter;
use Baxtian\WP_Importer\Files\ImporterReader;

use Baxtian\WP_Importer\Data\ExtractData;
use Baxtian\WP_Importer\Data\StructureImporter;
use Baxtian\WP_Importer\Data\ImportBatch;

use Exception;
use Timber\Attachment;

define('WP_IMPORTER_V', '0.3.25');

/**
 * Clase base para los Importadores
 */
class WP_Importer
{
	// DependencyInjection
	use \Baxtian\SingletonTrait;

	// Nombre estructura
	protected $tipo;
	protected $tipos;
	protected $accion;

	// Lista de campos
	protected $campos;

	// Mensaje de publicación
	protected $textos;

	// Items por página
	protected $items_per_page;

	// Habiliar importador de attachments
	protected $import_attachments;

	/**
	 * Constructor del importador
	 */
	public function __construct($arguments = [])
	{
		$clases = [
			'import_batch'       => ImportBatch::class,
			'extract_data'       => ExtractData::class,
			'structure_importer' => StructureImporter::class,
			'importer_page'      => ImporterPage::class,
			'render'             => Render::class,
			'importer_reader'    => ImporterReader::class,
			'exporter_writer'    => ExporterWriter::class,
		];

		$this->set_dependencies($arguments, $clases);
	}

	public function __get($name)
	{
		if (in_array($name, [
			'tipo', 'tipos', 'accion', 'campos', 'textos', 'items_per_page',
		])) {
			return $this->$name;
		}

		return false;
	}

	public function __set($name, $value)
	{
		if (in_array($name, [
			'tipo', 'tipos', 'accion', 'campos', 'textos', 'items_per_page',
		])) {
			$this->$name = $value;
		}

		return false;
	}

	/**
	 * Inicializar el enlace del importador y los ajax para exportar e importar
	 * @codeCoverageIgnore
	 */
	public function init()
	{
		add_action('admin_init', [$this, 'admin_init']);
		add_action('admin_init', [$this, 'i18n']);
		add_action('wp_ajax_export_' . $this->tipos, [$this, '_export']);
		add_action('wp_ajax_importer_' . $this->tipos, [$this, '_import']);

		if (empty($this->items_per_page)) {
			$this->items_per_page = 50;
		}

		if (empty($this->import_attachments)) {
			$this->import_attachments = false;
		}

		if($this->import_attachments) {
			Attachments::get_instance()->init();
		}
	}

	/**
	 * Importar el archivo de traducción
	 * @codeCoverageIgnore
	 */
	public function i18n()
	{
		$domain = 'wp_importer';
		$locale = apply_filters('plugin_locale', determine_locale(), $domain);

		$wp_component = basename(WP_CONTENT_URL);
		$_path        = explode($wp_component, __DIR__);
		$path         = dirname(WP_CONTENT_DIR . $_path[1]) . '/languages/';
		$mofile       = $path . $domain . '-' . $locale . '.mo';

		load_textdomain($domain, $mofile);
	}

	/**
	 * Incluir en el menú de importación
	 * @codeCoverageIgnore
	 */
	public function admin_init()
	{
		register_importer($this->accion, $this->textos['import_button'], $this->textos['description'], [$this, 'page']);
	}

	/**
	 * Renderizado de página de importación
	 */
	public function page()
	{
		$has_p2p = array_filter($this->campos, function ($value) {
			return ($value['type'] == 'p2p');
		});

		$has_p2p = (count($has_p2p) > 0);

		list($tpl, $args) = $this->dependency('importer_page')->getView($this->tipo, $this->tipos, $this->textos, $this->accion, $has_p2p);

		// Twig
		$this->dependency('render')->render($tpl, $args);
	}

	/**
	 * Llamado ajax para atender la importación
	 * @return string JSON con los datos de importación
	 */
	public function _import()
	{
		try {
			// Por defecto no habra siguiente paso de importación
			$answer = [
				'siguiente' => 0,
			];

			// Si envía el número de paso y hay archivo a importar
			if (isset($_GET['step']) && isset($_GET['filename']) && isset($_GET['filetype'])) {
				$filename   = sanitize_text_field($_GET['filename']);
				$filetype   = sanitize_text_field($_GET['filetype']);
				$p2p_action = sanitize_text_field($_GET['p2p_action']);
				$step       = sanitize_text_field($_GET['step']);

				$answer = $this->import($filename, $filetype, $p2p_action, $step);
			}
		} catch (Exception $e) {
			$answer = [
				'error'   => true,
				'message' => $e->getMessage(),
			];
		}

		echo json_encode($answer);
		wp_die();
	}

	/**
	 * Importación
	 * @return string JSON con los datos de importación
	 */
	public function import($filename, $filetype, $p2p_action, $step)
	{
		$file_reader = $this->dependency('importer_reader')->getImporter($filetype);

		// Datos para structure importer
		$structure_importer             = $this->dependency('structure_importer');
		$structure_importer->tipo       = $this->tipo;
		$structure_importer->campos     = $this->campos;
		$structure_importer->p2p_action = (empty($p2p_action)) ? StructureImporter::ONLY_P2P : $p2p_action;

		// Convertir archivo en arreglo
		$file_reader->setFile($filename);
		$all_data = $file_reader->fileToData();

		// Importación del lote actual
		$answer = $this->dependency('import_batch')->import($all_data, $structure_importer, $step, $this->items_per_page, $this->p2p_action);

		// Si hay respuesta, agregar el mensaje
		if (isset($answer['acciones']['nuevas'])) {
			$answer['mensaje'] = sprintf(
				$this->textos['mensaje_publicacion'],
				$answer['acciones']['nuevas'],
				$answer['acciones']['modificadas'],
				$answer['acciones']['terminos_nuevos']
			);
		}

		// Si no hay siguiente, borrar archivo
		if ($answer['siguiente'] == 0) {
			$file_reader->deleteFile();
		}

		return $answer;
	}

	/**
	 * Llamado ajax para atender la exportación
	 * @return string JSON con los datos de importación
	 */
	public function _export()
	{
		try {

			// Tipo de archivo a exportar
			$type = (isset($_GET['type'])) ? sanitize_text_field($_GET['type']) : 'csv';

			$this->export($type);
		} catch (Exception $e) {
			$answer = [
				'error'   => true,
				'message' => $e->getMessage(),
			];
			echo json_encode($answer);
		}

		wp_die();
	}

	/**
	 * Exportación
	 */
	public function export($type)
	{
		// Exportador
		$file_writer = $this->dependency('exporter_writer')->getExporter($type);

		// Extraer los datos
		$data = $this->dependency('extract_data')->export($this->tipo, $this->tipos, $this->campos);

		// Convertir archivo en arreglo
		$file_writer->dataToFile($this->tipos, $data);
	}
}
