<?php

namespace Baxtian\WP_Importer\Files;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception as XException;
use Exception;

/**
 * Clase base para exportar archivo XLSX
 */
class ImportXLSX implements ImportFileInterface
{
	protected $filename;

	/**
	 * Undocumented function
	 *
	 * @param string $filename Filename and path
	 * @return void
	 */
	public function setFile($filename)
	{
		$this->filename = $filename;
	}

	/**
	 * Determnar si el archivo existe
	 *
	 * @return void
	 */
	private function checkFile()
	{
		if (
			empty($this->filename) ||
			!file_exists($this->filename)
		) {
			throw new Exception(__('No file detected.', 'wp_importer'));
		}
	}

	/**
	 * Función para convertir un archivo xlsx o xls en un arreglo
	 * @return array             Arreglo con los datos del archivo Excel
	 */
	public function fileToData()
	{
		// Revisar el archivo
		$this->checkFile();

		// Leer el archivo
		try {
			$spreadsheet = IOFactory::load($this->filename);
		} catch (XException $e) {
			throw new Exception(__('Error while reading XLSX file.', 'wp_importer'));
		}

		$worksheet = $spreadsheet->getActiveSheet();

		// Recorrer la hoja de cálculo y guardar los datos en arr
		$arr = [];
		foreach ($worksheet->getRowIterator() as $row) {
			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(false);

			// Recorrer todas las celdas,
			$vacio = true;
			$cells = [];
			foreach ($cellIterator as $cell) {
				$text    = $cell->getCalculatedValue();
				$cells[] = $text;
				if (!empty($text)) {
					$vacio = false;
				}
			}

			// Asignar todos los datos de celdas
			// como fila del arreglo de datos si
			// al menos una celda tenía texto
			if (!$vacio) {
				$arr[] = $cells;
			}
		}

		return $arr;
	}

	/**
	 * Borra el archivo
	 *
	 * @return void
	 */
	public function deleteFile()
	{
		// Revisar el archivo
		$this->checkFile();

		// Eliminar el archivo
		unlink($this->filename);
	}
}
