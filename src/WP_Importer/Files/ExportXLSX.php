<?php

namespace Baxtian\WP_Importer\Files;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Exception;

/**
 * Clase base para exportar archivo XLSX
 * @codeCoverageIgnore
 */
class ExportXLSX implements ExportFileInterface
{
	/**
	 * Crea el archivo XLSX para descargar
	 *
	 * @param string $filename Nombre del archivo
	 * @param array  $data     Datos a almacenar
	 * @return void
	 */
	public function dataToFile($filename, $data)
	{
		try {
			// Encabezados para descarga
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
			header('Cache-Control: no-cache, no-store, must-revalidate');
			header('Pragma: no-cache');
			header('Expires: 0');

			// Imprimir archivo XLSX
			$spreadsheet = new Spreadsheet();
			$sheet       = $spreadsheet->getActiveSheet();

			$sheet->fromArray($data, null, 'A1');

			$writer = new Xlsx($spreadsheet);
			$writer->save('php://output');
		} catch (Exception $e) {
			throw new Exception(__('Error while exporting XLSX file.', 'wp_importer'));
		}
	}
}
