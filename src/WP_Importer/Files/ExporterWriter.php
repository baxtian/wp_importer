<?php

namespace Baxtian\WP_Importer\Files;

use Exception;
use Baxtian\WP_Importer\Files\ExportCSV;
use Baxtian\WP_Importer\Files\ExportXLSX;

/**
 * Clase base para exportar archivo CSV
 */
class ExporterWriter
{
	/**
	 * Función para convertir un archivo csv en un arreglo
	 * @return array             Arreglo con los datos del CSV
	 */
	public function getExporter($extension)
	{
		if (in_array($extension, ['xlsx', 'xls'])) {
			return new ExportXLSX();
		} elseif ($extension == 'csv') {
			return new ExportCSV();
		}

		// Extensión desconocida
		throw new Exception(__('Unrecognized type.', 'wp_importer'));
	}
}
