<?php

namespace Baxtian\WP_Importer\Files;

// COnvierte arreglo en archivo
interface ExportFileInterface
{
	public function dataToFile($filename, $data);
}
