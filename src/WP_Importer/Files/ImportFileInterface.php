<?php

namespace Baxtian\WP_Importer\Files;

// COnvierte arreglo en archivo
interface ImportFileInterface
{
	public function setFile($filename);
	public function fileToData();
	public function deleteFile();
}
