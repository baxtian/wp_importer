<?php

namespace Baxtian\WP_Importer\Files;

use Exception;

/**
 * Clase base para exportar archivo CSV
 */
class ImportCSV implements ImportFileInterface
{
	protected $filename;

	/**
	 * Undocumented function
	 *
	 * @param string $filename Filename and path
	 * @return void
	 */
	public function setFile($filename)
	{
		$this->filename = $filename;
	}

	/**
	 * Determinar si el archivo existe
	 *
	 * @return void
	 */
	private function checkFile()
	{
		if (
			empty($this->filename) ||
			!file_exists($this->filename)
		) {
			throw new Exception(__('No file detected.', 'wp_importer'));
		}
	}

	/**
	 * Función para convertir un archivo csv en un arreglo
	 * @return array             Arreglo con los datos del CSV
	 */
	public function fileToData()
	{
		// Revisar el archivo
		$this->checkFile();

		$delimiter = ',';
		$enclosure = '"';

		$arr = [];

		if (($handle = fopen($this->filename, 'r')) !== false) {
			$encoding = mb_detect_encoding(file_get_contents($this->filename), mb_list_encodings());
			$i        = 0;
			while (($lineArray = fgetcsv($handle, 0, $delimiter, $enclosure)) !== false) {
				if ($encoding != 'UTF-8') {
					$lineArray = array_map('utf8_encode', $lineArray);
				}
				if (count($lineArray) == 1) {
					continue;
				} //skip empty line (will fail if file will have only one column)
				for ($j = 0; $j < count($lineArray); $j++) {
					$arr[$i][$j] = trim($lineArray[$j]); //trim value
				}
				$i++;
			}
			fclose($handle);
		}

		return $arr;
	}

	/**
	 * Borra el archivo
	 *
	 * @return void
	 */
	public function deleteFile()
	{
		// Revisar el archivo
		$this->checkFile();

		unlink($this->filename);
	}
}
