<?php

namespace Baxtian\WP_Importer\Files;

use Exception;
use Baxtian\WP_Importer\Files\ImportCSV;
use Baxtian\WP_Importer\Files\ImportXLSX;

/**
 * Clase base para exportar archivo CSV
 */
class ImporterReader
{
	/**
	 * Función para obtener el importador
	 * @return ImportFileInterface Instancia del importador
	 */
	public function getImporter($extension)
	{
		if (in_array($extension, ['xlsx', 'xls'])) {
			return new ImportXLSX();
		} elseif ($extension == 'csv') {
			return new ImportCSV();
		}

		// Extensión desconocida
		throw new Exception(__('Unrecognized type.', 'wp_importer'));
	}
}
