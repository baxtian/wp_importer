<?php

namespace Baxtian\WP_Importer\Files;

use Exception;

/**
 * Clase base para exportar archivo CSV
 * @codeCoverageIgnore
 */
class ExportCSV implements ExportFileInterface
{
	/**
	 * Crea el archivo CSV para descargar
	 * @param  string $filename Nombre del archivo
	 * @param  array  $data     Datos a almacenar
	 * @return void
	 */
	public function dataToFile($filename, $data)
	{
		try {
			// Crear archivo CSV
			$output = fopen('php://memory', 'r+');
			foreach ($data as $item) {
				fputcsv($output, $item);
			}
			rewind($output);

			// Encabezados para descarga
			header('Content-Type: text/csv');
			header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
			header('Cache-Control: no-cache, no-store, must-revalidate');
			header('Pragma: no-cache');
			header('Expires: 0');

			// Imprimir archivo CSV
			echo stream_get_contents($output);

			// Cerrar el administrador del archivo
			fclose($output);
		} catch (Exception $e) {
			throw new Exception(__('Error while exporting CSV file.', 'wp_importer'));
		}
	}
}
