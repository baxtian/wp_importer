<?php

namespace Baxtian\WP_Importer\Data;

use Baxtian\WP_Importer as WP_Importer;

/**
 * Componente para importar películas
 */
class Attachments extends WP_Importer
{
	use \Baxtian\SingletonTrait;

	private $active;

	/**
	 * Inicializa el componente
	 */
	protected function __construct()
	{
		if($this->active) return false;
		
		add_action('init', [$this, 'init']);
		$this->active = true;

		parent::__construct();
	}

	/**
	 * Declarar los datos para la importación.
	 * Se hace en esta función para dar tiempo a cargar las traducciones.
	 */
	public function init()
	{
		$this->tipo   = 'attachment';
		$this->tipos  = 'attachments';
		$this->accion = 'import_attachments';

		// Indicar el número de items por página e importación
		// Esto se hace para estructuras muy grandes
		$this->items_per_page = 5;

		$this->campos = [
			// Determine en este arreglo las columnas de datos que va a importar
			// Los tipos disponibles son
			//	id: es el campo ID de la entrada
			//	field: campo normal de una entrada
			//	term: taxonomía vinculada a la entrada
			//	postmeta: dato vinculado a la entrada por postmeta
			//	postmeta_textarea: datos tipo html y textarea
			//	postmeta_date: dato tipo fecha
			//	postmeta_datetime: dato tipo fecha y hora
			//	postmeta_bool: dato vinculado a la entrada por post_meta y que es
			//					de tipo booleano
			// Por defecto deje estos dos campos iniciales por tratarse de
			// los elementos básicos que se necesita para hacer la importación
			// ***** Si un elemento de tipo 'field' tiene un campo 'postmeta',
			// entonces ese valor se almacenará tanto en el 'field' como en
			// el 'postmeta'
			//****************************//
			[
				'name' => 'id',
				'type' => 'id',
			],
			[
				'name'     => 'post_title',
				'type'     => 'field',
			],
			[
				'name'     => 'post_name',
				'type'     => 'field',
			],
			[
				'name' => 'post_date',
				'type' => 'field',
			],
			[
				'name' => 'guid',
				'type' => 'field',
			]
		];

		$this->i18n();

		// Textos que se mostrarán en el proceso.
		$this->textos = [
			'mensaje_publicacion' => __('Attachments added: %d, Attachments updated: %d, Terms added %d', 'wp_importer'),
			'import_button'       => __('Import Attachments', 'wp_importer'),
			'import_file'         => __('Attachments Archive', 'wp_importer'),
			'plural'              => __('attachments', 'wp_importer'),
			'description'         => __('Import Attachments from a file.', 'wp_importer'),
		];

		//Ya con las variables podemos inicializar los llamados a las funciones
		parent::init();
	}
}
