<?php

namespace Baxtian\WP_Importer\Data;

use Exception;

class ImportBatch
{
	/**
	 * Función para importar el archivo. Para evitar que se supere el tiempo del
	 * servidor se hace la importación por grupos.
	 * Por defecto estos grupos son de 20 elementos
	 * @param  array   $all_data			Datos a importar
	 * @param  object  $structureImporter 	Instancia del importador de estructura
	 * @param  integer $step     		  	Grupo que se importará.
	 * @param  integer $items_per_page	  	Número de items por grupo.
	 * @param  string  $p2p_action	  		Cómo serán las acciones sobre relacions p2p.
	 * @return array            		  	Datos de la importación.
	 *          		       		      		array(
	 *          		       		        		primero: posición en lista del primer item importado de este grupo
	 *          		       		        	 	ultimo: posición en lista del último item importado de este grupo
	 *          		       		        	 	siguiente: número del siguiente grupo a importar.
	 *          		       		        	 			   False para indicar que no hay más grupos
	 *          		       		        	 	mensajes: array(
	 *          		       		        	 		mensaje_publicacion: Mensaje con los datos importados
	 *          		       		        	 		nuevas: cantidad de nuevos items agregados.
	 *          		       		        	 		modificadas: cantidad de items modificados.
	 *          		       		        	 		terminos_nuevos: número de términos que se crearon
	 *          		       		        		)
	 *          		       		        	)
	 */
	public function import($all_data, $structureImporter, $step = 1, $items_per_page = 50, $p2p_action = 'sin')
	{
		// Vincular cada elemento con el label para facilitar
		// la identificación durante la importación
		array_walk($all_data, function (&$a) use ($all_data) {
			$a = array_combine($all_data[0], $a);
		});

		// Eliminar la primera fila por tratarse de la fila con los nombres de las columnas.
		array_shift($all_data);

		// Obtener el lote que se va a importar
		$data_to_import = array_slice($all_data, ($step - 1) * $items_per_page, $items_per_page);

		// Correr por el importador de la estructura
		$acciones = $structureImporter->importData($data_to_import);

		// Total a importar
		$total_items = count($all_data);

		// Datos de rango
		$primero = (($step - 1) * $items_per_page);
		$ultimo  = (($step - 1) * $items_per_page) + $items_per_page - 1;

		// ¿Quedan aun por importar?
		$pendientes = ($ultimo >= $total_items - 1) ? false : true;

		$answer = [
			'acciones'  => $acciones,
			'primero'   => $primero,
			'ultimo'    => $ultimo,
			'siguiente' => ($pendientes) ? $step + 1 : 0,
		];

		// Retornar acciones realizadas
		return $answer;
	}
}
