<?php

namespace Baxtian\WP_Importer\Data;

use Timber\Timber;
use DateTime;
use WP_Http;

class StructureImporter
{
	const ONLY_P2P    = 1000;
	const WITH_P2P    = 1001;
	const WITHOUT_P2P = 1002;

	// Variables
	protected $tipo;
	protected $campos;
	protected $taxonomias;
	protected $acciones;
	private $media      = false;
	private $p2p_action = self::WITHOUT_P2P;

	/**
	 * Constructor del estructura de importador
	 */
	public function __construct()
	{
		// Inicializar variables
		$this->taxonomias = [];
		$this->acciones   = [
			'nuevas'          => 0,
			'modificadas'     => 0,
			'terminos_nuevos' => 0,
			'mensajes'        => [],
		];
	}

	public function __get($name)
	{
		$answer = false;
		switch ($name) {
			case 'tipo':
			case 'campos':
			case 'acciones':
			case 'taxonomias':
				$answer = $this->$name;

				break;
		}

		return $answer;
	}

	public function __set($name, $value)
	{
		switch ($name) {
			case 'tipo':
			case 'campos':
			case 'p2p_action':
				$this->$name = $value;

				break;
		}
	}

	/**
	 * Correr el sistema de importación diferenciando
	 * si es archivo adjunto o otra estructura.
	 *
	 * @param array $data
	 * @return array
	 */
	public function importData($data)
	{
		return ($this->tipo == 'attachment') ?
			$this->importDataAttachment($data) :
			$this->importDataStructure($data);
	}

	/**
	 * Los títulos al ser guardados cmabian & por &amp;
	 * Esta función hace ese reemplazo para poder confirmar
	 * que el título importado coincida con el título
	 * almacenado
	 *
	 * @param string $title
	 * @return string
	 */
	private function correctTitle($title)
	{
		// Corregir los & de los títulos
		$title = str_replace('&', '&amp;', $title);
		$title = str_replace('&amp;amp;', '&amp;', $title);

		return $title;
	}

	/**
	 * Importa los datos del arreglo data en una estructura tipo, según la configuración almacenada en
	 * la variable campos y retorna un arreglo con las acciones realizadas.
	 *
	 * @param array  $data     Datos de la estructura a ser almacenadaso
	 * @return array
	 */
	private function importDataStructure($data)
	{
		global $wpdb;
		$tipo   = $this->tipo;
		$campos = $this->campos;

		// Recorrer el arreglo de datos
		foreach ($data as $line) {

			// Usar esta primera para determinar si se incluyen todos los datos
			// que requerimos para crear los registros.

			// Solo intentar la importación si tenemos disponibilidad del título
			if (isset($line['post_title']) && !empty($line['post_title'])) {
				// ¿Trae ID este archivo y no está vacío? Entonces usar
				if (isset($line['id']) && !empty($line['id'])) {
					// Determinar que el ID sí existe,
					// está publicado y es del tipo esperado
					$post_id = false;
					$sql     = "SELECT ID
						FROM $wpdb->posts
						WHERE ID = '%d'
							AND post_type = '$tipo'
							AND post_status = 'publish'";
					$post_id = $wpdb->get_var($wpdb->prepare($sql, $line['id']));
				} else {
					// Buscar el ID de la estructura que ya tengamos registrada con ese nombre
					$post_id = false;

					$sql = "SELECT ID
						FROM $wpdb->posts
						WHERE post_title = '%s'
							AND post_type = '$tipo'
							AND post_status = 'publish'";
					$post_id = $wpdb->get_var($wpdb->prepare($sql, $this->correctTitle($line['post_title'])));
				}

				// ¿Hay un id?
				if (!$post_id) {
					// Si no hay un ID para la estructura con ese nombre, entonces crearla
					// Crear el registro con el título, del tipo de la 'estructura' y en estado 'publicado'.
					$args = [
						'post_title'  => $line['post_title'],
						'post_type'   => $tipo,
						'post_status' => 'publish',
					];
					// Crear la estructura y guardar el id
					$post_id = wp_insert_post($args);

					// Registrar la acción
					$this->acciones['nuevas']++;
				} else {
					// Si tenemos un ID entonces es porque la estructura ya existe.
					// Por lo tanto usaremos ese ID para las acciones sobre esa estructura

					// Registrar la acción
					$this->acciones['modificadas']++;
				}

				// Ir creando el elemento de my_post por si hay un cambio en los datos de la estructura
				$my_post = [];

				// Recorrer el arreglo de campos y guardar según el tipo
				foreach ($campos as $campo) {
					$name = $campo['name'];
					switch ($campo['type']) {
						case 'field':
							// Si es solo los P2P cortar,
							if($this->p2p_action == self::ONLY_P2P) {
								break;
							}

							// Solo continuar si el arreglo de datos dispone de esta variable
							if (isset($line[$name])) {
								// Los campos de tipo field se guardarán en conjunto al terminar de leer
								// las columnas de esta fila
								$my_post[$name] = $line[$name];
							}

							// ¿Hay un postmeta en este campo?
							if (isset($campo['postmeta'])) {
								update_post_meta($post_id, $campo['postmeta'], sanitize_text_field($line[$name]));
							}

							break;
						case 'thumbnail':
							// Si es solo los P2P cortar,
							if($this->p2p_action == self::ONLY_P2P) {
								break;
							}

							// Buscar la imagen
							if($val = $this->filter_media($line[$name])) {
								set_post_thumbnail($post_id, $val);
							}

							break;
						case 'term':
							// Si es solo los P2P cortar,
							if($this->p2p_action == self::ONLY_P2P) {
								break;
							}

							// Solo continuar si el arreglo de datos dispone de esta variable
							if (isset($line[$name])) {
								// Determinar si es una taxonomía que incluye comas en su texto
								$usar_separador = (isset($campo['entire_text']) && $campo['entire_text'] === true) ? false : true;

								// Obtener arreglo con ID de los términos en la variable
								$terms = (empty($line[$name])) ? '' : $this->get_term($name, $line[$name], $usar_separador);

								// Reemplazar los términos de esta entrada con los obtenidos
								// desde el arreglo de datos
								wp_set_post_terms($post_id, $terms, $name);
							}

							break;
						case 'postmeta':
							// Si es solo los P2P cortar,
							if($this->p2p_action == self::ONLY_P2P) {
								break;
							}

							// Solo continuar si el arreglo de datos dispone de esta variable
							if (isset($line[$name])) {
								// Si tiene activo el atributo 'media' hay que filtrar el valor
								// para convertir los slugs en valores.
								$val = (isset($campo['media']) && $campo['media'] == true) ?
									$this->filter_media($line[$name]) :
									$line[$name];

								// Almacenar directamente como postmeta
								update_post_meta($post_id, $name, sanitize_text_field($val));
							}

							break;
						case 'postmeta_textarea':
							// Si es solo los P2P cortar,
							if($this->p2p_action == self::ONLY_P2P) {
								break;
							}

							// Solo continuar si el arreglo de datos dispone de esta variable
							if (isset($line[$name])) {
								$allowed_html = [
									'a' => [
										'href' => [],
									],
									'img' => [
										'src'    => [],
										'width'  => [],
										'height' => [],
									],
									'br'  => [],
									'ul'  => [],
									'ul'  => [],
									'li'  => [],
									'ins' => [
										'datetime' => [],
									],
									'span' => [
										'style' => [],
									],
									'em'         => [],
									'blockquote' => [],
									'del'        => [],
									'code'       => [],
									'p'          => [
										'style' => [],
									],
									'strong' => [],
								];
								// Almacenar directamente como postmeta
								$value = wp_kses($line[$name], $allowed_html);
								update_post_meta($post_id, $name, $value);
							}

							break;
						case 'postmeta_date':
						case 'postmeta_datetime':
							// Si es solo los P2P cortar,
							if($this->p2p_action == self::ONLY_P2P) {
								break;
							}

							// Solo continuar si el arreglo de datos dispone de esta variable
							if (isset($line[$name])) {
								$value = '';

								// Si no e sun valor vacio, convertir la fecha
								if (!empty($line[$name])) {
									// Convertir fecha y almacenarla
									$date = new DateTime($line[$name]);

									if ($campo['type'] == 'postmeta_date') {
										$value = $date->format('Y-m-d');
									}

									if ($campo['type'] == 'postmeta_datetime') {
										$value = $date->format('Y-m-d H:i:s');
									}
								}
								update_post_meta($post_id, $name, $value);
							}

							break;
						case 'postmeta_bool':
							// Si es solo los P2P cortar,
							if($this->p2p_action == self::ONLY_P2P) {
								break;
							}

							// Solo continuar si el arreglo de datos dispone de esta variable
							if (isset($line[$name])) {
								// Convertir en booleano (1 o 0) y almacenar en postmeta
								update_post_meta($post_id, $name, (bool) ($line[$name]));
							}

							break;
						case 'p2p':
							// Si no se incluyen los P2P cortar
							if($this->p2p_action == self::WITHOUT_P2P) {
								break;
							}

							// Solo continuar si el arreglo de datos dispone de esta variable
							if (isset($line[$name])) {
								$relationships = $campo['p2p']->relationships;
								if(isset($relationships[$name])) {
									$relationship = $relationships[$name];

									$post_type = $this->tipo;
									if($relationship['from']['type'] == $post_type) {
										$post_type = $relationship['to']['type'];
									} elseif($relationship['to']['type'] == $post_type) {
										$post_type = $relationship['from']['type'];
									}

									// Convertir los slugs en ids
									$posts = $this->p2p_slugs2posts($line[$name], $post_type);

									if($posts == '') {
										break;
									} elseif($posts === false) {
										$this->accions['mensajes'][] = sprintf('Error creating relationship %s for post %d', $campo[$name], $post_id);
									} else {
										$campo['p2p']->set_relation($campo['name'], (int) $post_id, $posts);
									}
								}

							}

							break;
					}
				}

				// Hay datos en el my_post?
				if (count($my_post) > 0) {
					$my_post['ID'] = $post_id;
					wp_update_post($my_post);
				}

				// Acciones a ejecutar después de incluir todos los campos de una fila
				do_action('MrkImporter/row_added', $post_id, $tipo, $line);
			}
		}

		return $this->acciones;
	}

	/**
	 * Agregar la imagen al directorio de Uploads de Wordpress
	 *
	 * @param array $image
	 * @param string $title
	 * @return integer
	 */
	private function attach_image($image, $title)
	{
		$file_path     = $image['file'];
		$file_name     = basename($file_path);
		$file_type     = wp_check_filetype($file_name, null);
		$wp_upload_dir = wp_upload_dir();

		$post_info = [
			'guid'           => $wp_upload_dir['url'] . '/' . $file_name,
			'post_mime_type' => $file_type['type'],
			'post_title'     => $title,
			'post_content'   => '',
			'post_status'    => 'inherit',
		];
		$attach_id = wp_insert_attachment($post_info, $file_path);

		// Include image.php.
		require_once ABSPATH . 'wp-admin/includes/image.php';

		// Generate the attachment metadata.
		$attach_data = wp_generate_attachment_metadata($attach_id, $file_path);

		// Assign metadata to attachment.
		wp_update_attachment_metadata($attach_id, $attach_data);

		return $attach_id;
	}

	/**
	 * Importa los datos del arreglo data en una estructura attachment, según
	 * la configuración almacenada en la variable campos y retorna un arreglo
	 * con las acciones realizadas.
	 *
	 * @param array  $data     Datos de la estructura a ser almacenadaso
	 * @return array
	 */
	private function importDataAttachment($data)
	{
		global $wpdb;
		$tipo   = $this->tipo;
		$campos = $this->campos;

		// Recorrer el arreglo de datos
		foreach ($data as $line) {

			// La importación de una imagen solo se hace si
			// 1. no hay id asignado
			// 2. el post_title no se ha subido y
			// 3. está el campo guid disponibles en el item

			if (
				(isset($line['id']) && empty($line['id'])) &&
				(isset($line['post_title']) && !empty($line['post_title'])) &&
				(isset($line['guid']) && !empty($line['guid']))
			) {
				// Buscar si ya hay un attachment con este post_title
				$posts = Timber::get_posts([
					'post_type'   => 'attachment',
					'post_status' => 'any',
					'title'       => $line['post_title'],
				]);

				if($posts->count() == 0) {
					// Convertir el title en slug
					$line['post_name'] = (isset($line['post_name']) && !empty($line['post_name'])) ? $line['post_name'] : sanitize_title($line['post_title']);

					// Si la fecha está vacía usar la hora actual
					$line['post_date'] = (isset($line['post_date']) && !empty($line['post_date'])) ? $line['post_date'] : date_i18n('Y-m-d H:i:s');

					$image = $this->upload_url($line['guid'], $line['post_date']);

					if($image) {

						$this->attach_image($image, $line['post_title']);

						// Incrementar el indicador de nuevas
						$this->acciones['nuevas']++;
					}
				}
			}
		}

		return $this->acciones;
	}

	/**
	 * Obtener el contenido del archivo, sea una URL o un archivo local
	 *
	 * @param string $url URL de la imagen en línea o archivo local
	 * @return string Contenido
	 */
	private function get_content($url)
	{
		$content = false;

		// Determinar contenido según si es URL o archivo
		if(file_exists($url)) {
			$content = file_get_contents($url);
		} else {
			if (! class_exists('WP_Http')) {
				require_once ABSPATH . WPINC . '/class-http.php';
			}

			if(!str_contains($url, '//')) {
				$url = content_url('uploads/' . $url);
				if(!file_exists($url)) {
					return false;
				}
			}

			$http     = new WP_Http();
			$response = $http->request($url);
			if (200 !== $response['response']['code']) {
				return false;
			}

			$content = $response['body'];
		}

		return $content;
	}

	/**
	 * Subir imagen desde url.
	 *
	 * @param string  $url				La URL de l aimagen.
	 * @param string  $datetime			La fecha asignada para creación
	 * @return array|false				Los datos de la imagen subida
	 * 									False en caso de error.
	 */
	private function upload_url($url, $datetime)
	{
		// Obtener el contenido
		$content = $this->get_content($url);

		// Si no hay contenido, retornar falso
		if(empty($content)) {
			return false;
		}

		$date  = date_create($datetime);
		$title = urldecode(pathinfo($url, PATHINFO_BASENAME));

		$upload = wp_upload_bits($title, null, $content, date_format($date, 'Y/m'));
		if (! empty($upload['error'])) {
			return false;
		}

		return $upload;
	}

	/**
	 * Filtra la lista de medios bajo el supuesto de que está formada
	 * por url o archivos locales
	 *
	 * @param string $value
	 * @return string
	 */
	private function filter_media_urls($value)
	{
		$ids = [];
		// Extraer todas las URI
		$uris = explode(',', $value);

		// Importar cada uri
		foreach($uris as $uri) {
			$title = urldecode(pathinfo($uri, PATHINFO_FILENAME));

			// Buscar si ya existe una imagen con este slug
			$args = [
				'post_type'      => 'attachment',
				'name'           => sanitize_title($title),
				'posts_per_page' => 1,
				'post_status'    => 'inherit',
			];
			$_header = get_posts($args);
			$image   = $_header ? array_pop($_header) : null;

			if($image) {
				// Si ya está la imagen, incluirla en la lista de ids
				$ids[] = $image->ID;
			} else {
				// Crear la imagen
				$image = $this->upload_url($uri, date_i18n('Y-m-d H:i:s'));

				// Incluir en la lista de ids el id de la imagen creada
				$ids[] = $this->attach_image($image, $title);
			}
		}

		return implode(',', $ids);
	}

	/**
	 * Filtra la lista de medios bajo el supuesto de que está formada por slugs
	 *
	 * @param string $value
	 * @return string
	 */
	private function filter_media_slugs($value)
	{
		// Buscar texto, guiones, números y comas
		preg_match_all('![0-9a-z\-\,]+!', $value, $matches);
		// Si hay al menos uno, consultar
		if(count($matches) == 1) {
			// El valor es el elemento encontrado
			$match = $matches[0][0];
			$ids   = explode(',', $match);

			// Si hay al menos un valor, iniciar la conversión
			if(count($ids) > 0) {
				$media = $this->get_media();

				foreach($ids as $key => $id) {
					$ids[$key] = $media[$id] ??
						$id;
				}
			}

			// Volver a reconectar
			$ids = implode(',', $ids);

			// Reemplazar
			$value = str_replace($match, $ids, $value);
		}

		return $value;
	}

	/**
	 * Convierte un texto de slugs separados por comas en los ids
	 * de las imágenes vinculadas.
	 *
	 * @param string $value	Texto con slugs separados por comas
	 * @return string		Texto con ids separados por comas
	 */
	private function filter_media($value)
	{
		// Si está vació retornar
		if(empty($value)) {
			return $value;
		}

		if(strstr($value, '/') === false) {
			$value = $this->filter_media_slugs($value);
		} else {
			$value = $this->filter_media_urls($value);
		}

		return $value;
	}

	/**
	 * A partir de un texto que puede o no estar separado por comas determina el
	 * term_id de cada uno al mismo tiempo que irá creando los términos que no existan.
	 * Retorna un arreglo con los identificadores de cada término.
	 * @param  string  $taxonomia Taxonomía de los términos.
	 * @param  string  $texto     Nombre del término.
	 * @param  boolean $separador Indicar si se debe usar ',' para separar el texto en
	 *                            varios términos
	 * @return array              Arreglo con los identificadores de los términos
	 */
	private function get_term($taxonomia, $texto, $separador = true)
	{
		// Inicializar la respuesta
		$answer = [];

		// Si estamos usando la coma como separador
		if ($separador) {
			// Extraer todos los términos en el texto (lista separada por coma) y retirar
			// los espacios al inicio y al fin
			$terms = array_map('trim', explode(',', $texto));
		} else {
			// Retirar los espacios al inicio y al fin
			$terms = [trim($texto)];
		}

		// Crear el arreglo de la taxonomía si no está en la lista de taxonomías
		if (!isset($this->taxonomias[$taxonomia])) {
			$this->taxonomias[$taxonomia] = [];
		}

		// Recorrer la lista de términos
		foreach ($terms as $item) {
			// Estandarizar los términos
			// Mayúscula en la primera letra de cada palabra
			$item = ucwords(trim($item));
			// Si aun no tenemos registro de ese término entonces hay que crearlo
			if (empty($this->taxonomias[$taxonomia][$item])) {
				//Buscar ese término en los registros de la taxonomía correspondiente
				$term = get_term_by('name', $item, $taxonomia);

				// ¿Existe registro de ese término en la taxonomía?
				if ($term) {
					// Si encontramos el término registrarlo en el arreglo de taxonomías
					// Usar como llave el texto del nombre
					$this->taxonomias[$taxonomia][$item] = $term->term_id;
				} else {
					// Si no existe registro del término entonces se crea el término en la taxonomía
					$term = wp_insert_term($item, $taxonomia);

					// ¿Se pudo crear?
					if (!is_wp_error($term)) {
						// Guardar el registro en el arreglod e taxonomías y usar el texto del nombre como llave
						$this->taxonomias[$taxonomia][$item] = $term['term_id'];
						// Guardar registro de la acción
						$this->acciones['terminos_nuevos']++;
					}
				}
			}

			// Incluir en respuesta lo que tenemos en el arreglo de taxonomía para esa llave
			// en caso de existir o haber sido creada
			if (isset($this->taxonomias[$taxonomia][$item])) {
				$answer[] = $this->taxonomias[$taxonomia][$item];
			}
		}

		// Retornar respuesta
		return $answer;
	}

	/**
	 * Descarga la lista de medios. Crea el arreglo en caso
	 * de no haber sido creado aun.
	 * El arreglo tiene como llave los slugs de las
	 * imágenes y cada slug se relaciona con el id de la imagen.
	 *
	 * @return void
	 */
	private function get_media()
	{
		// Si aun no tenemos los id de medios,
		// crear el arreglo
		if($this->media == false) {
			$media = Timber::get_posts([
				'post_type'   => 'attachment',
				'post_status' => 'any',
				'nopaging'    => true,
			]);

			foreach($media->to_array() as $item) {
				$this->media[$item->post_name] = $item->id;
			}
		}

		return $this->media;
	}

	private function p2p_slugs2posts($string, $post_type)
	{
		if(trim($string) == '') {
			return '';
		}

		$slugs  = explode(',', $string);
		$answer = [];

		foreach($slugs as $slug) {
			$posts = get_posts([
				'post_type'      => $post_type,
				'posts_per_page' => 1,
				'title'          => $slug,
				'orderby'        => 'title',
			]);
			if(count($posts) > 0) {
				$aux      = array_shift($posts);
				$answer[] = $aux->ID;
			}
		}

		// Si difieren entonces algo está mal
		if(count($slugs) != count($answer)) {
			return false;
		}

		return $answer;
	}
}
