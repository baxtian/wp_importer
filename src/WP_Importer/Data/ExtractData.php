<?php

namespace Baxtian\WP_Importer\Data;

use Timber\Timber;

class ExtractData
{
	private $media = false;

	/**
	 * Crea el archivo con los datos
	 *
	 * @param  object $importer  Datos del importador
	 * @param  string $extension Extensión del archivo a crear
	 * @return void
	 */
	public function export($tipo, $tipos, $campos)
	{
		$args = [
			'post_type' => $tipo,
			'nopaging'  => true,
		];

		// Si son adjuntos, usar cualquier estado
		if($tipo == 'attachment') {
			$args['post_status'] = 'any';
		}

		// Datos de las estructuras
		$posts = Timber::get_posts($args);
		// Inicializar arreglo de datos
		$data = [];

		// Quitar el ID si no hay datos
		if (count($posts) == 0) {
			array_shift($campos);
		}

		// Crear fila de nombres
		$first_row = [];
		foreach ($campos as $item) {
			$first_row[] = $item['name'];
		}
		$data[] = $first_row;

		// Recorrer las filas
		foreach ($posts as $post) {
			$row = [];
			// Recorrer los campos y agregar el elemento al arreglo de la fila
			foreach ($campos as $item) {
				$field_name = $item['name'];
				if ($item['type'] == 'term') { // Si es una taxonomía
					$terms = $post->terms($field_name);
					array_walk($terms, function (&$a) use ($terms) {
						$a = $a->name;
					});
					$row[] = implode(', ', $terms);
				} elseif ($item['type'] == 'postmeta') { // Si es un postmeta
					// Si tiene activo el atributo 'media' hay que filtrar el valor
					// para convertir los slugs en valores.
					$row[] = (isset($item['media']) && $item['media'] == true) ?
						$this->filter_media($post->$field_name) :
						$post->$field_name;

				} elseif ($item['type'] == 'postmeta_bool') { // Si es un postmeta
					$row[] = ($post->$field_name) ? 1 : 0; // Y además es booleano
				} elseif ($item['type'] == 'p2p') { // Si es un postmeta
					$related_posts = $this->related_posts($post->id, $item['name']);
					$row[] = $related_posts;
				} else {
					$row[] = $post->$field_name;
				}
			}
			$data[] = $row;
		}

		return $data;
	}

	/**
	 * Convierte un texto de ids separados por comas en los slugs
	 * de las imágenes vinculadas.
	 *
	 * @param string $value	Texto con ids separados por comas
	 * @return string		Texto con slugs separados por comas
	 */
	private function filter_media($value)
	{
		// Si está vació retornar
		if(empty($value)) {
			return $value;
		}

		// Buscar números y comas
		preg_match_all('![0-9\,]+!', $value, $matches);
		// Si hay al menos uno, consultar
		if(count($matches) == 1) {
			// El valor es el elemento encontrado
			$match = $matches[0][0];
			$ids   = explode(',', $match);

			// Si hay al menos un valor, iniciar la conversión
			if(count($ids) > 0) {
				$media = $this->get_media();

				foreach($ids as $key => $id) {
					$ids[$key] = $media[$id] ??
						$id;
				}
			}

			// Volver a reconectar
			$ids = implode(',', $ids);

			// Reemplazar
			$value = str_replace($match, $ids, $value);
		}

		return $value;
	}

	/**
	 * Descarga la lista de medios. Crea el arreglo en caso
	 * de no haber sido creado aun.
	 * El arreglo tiene como llave los ids de las
	 * imágenes y cada id se relaciona con el slug de la imagen.
	 *
	 * @return void
	 */
	private function get_media()
	{
		// Si aun no tenemos los id de medios,
		// crear el arreglo
		if($this->media == false) {
			$media = Timber::get_posts([
				'post_type'   => 'attachment',
				'post_status' => 'any',
				'nopaging'    => true,
			]);

			foreach($media->to_array() as $item) {
				$this->media[$item->id] = $item->post_name;
			}
		}

		return $this->media;
	}

	private function related_posts($post_id, $relationship) {
		$related_posts = get_posts(
			[
				'post_type' => 'any',
				'p2p' => [[
					'type' => $relationship,
					'to'   => $post_id,
				]],
				'nopaging' => true,
			]
		);

		return implode(',',array_map(function($item) {
			return $item->post_title;
		}, $related_posts));
	}
}
