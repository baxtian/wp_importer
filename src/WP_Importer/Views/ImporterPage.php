<?php

namespace Baxtian\WP_Importer\Views;

use Baxtian\WP_Importer\Data\StructureImporter;

/**
 * Clase base para exportar archivo CSV
 * @codeCoverageIgnore
 */
class ImporterPage
{
	/**
	 *  Generar la vista de la página de importación, ya sea el formulario o el proceso de importación.
	 *
	 * @param string $tipo		Tipo de estructura a importar
	 * @param string $tipos		Plural del tipo d e estructura a importar (se usa en el nombre del archivo)
	 * @param array  $textos	Arreglo con los textos a usar en el formulario
	 * @param string $accion	Acción que ejecutará el ajax
	 * @param bool  $has_p2p	Indica si ente los campos hay uno de tipo p2p
	 * @return array 			Plantilla y argumentos a ser renderizados
	 */
	public function getView(string $tipo, string $tipos, array $textos, string $accion, bool $has_p2p = false): array
	{
		// Directorios
		$wp_component = basename(WP_CONTENT_URL);
		$_path        = explode($wp_component, __DIR__);
		$url          = dirname(content_url($_path[1]), 3) . '/';

		$_path = explode($wp_component, $url);
		$path  = trailingslashit(WP_CONTENT_DIR . $_path[1] . 'languages/');

		// Scripts y estilos
		wp_register_script('importer', $url . 'assets/scripts/importer.js', ['jquery', 'jquery-ui-dialog'], WP_IMPORTER_V);
		wp_localize_script('importer', 'importer_var', ['step' => __('Step', 'wp_importer'), 'finished' => __('Finished', 'wp_importer')]);
		wp_enqueue_script('importer');

		// Si no tenemos registro o encolamiento de jquery-ui, agregarlo
		if (!wp_style_is('jquery-ui', 'registered') && !wp_style_is('jquery-ui', 'enqueued')) {
			wp_register_style('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css', [], WP_IMPORTER_V);
		}

		wp_register_style('importer', $url . 'assets/css/importer.css', ['jquery-ui'], WP_IMPORTER_V);
		wp_enqueue_style('importer');

		// Datos
		$args = [];

		$args['importar'] = false;
		$args['has_p2p']  = $has_p2p;
		$args['p2p_action'] = [
			'with' => StructureImporter::WITH_P2P,
			'without' => StructureImporter::WITHOUT_P2P,
			'only' => StructureImporter::ONLY_P2P,
		];
		$tpl              = 'importers/form.twig';

		// ¿Hay un archivo para importar?
		if (isset($_FILES['import_file']) && $_FILES['import_file']['size'] > 0) {
			// Al parecer estamos importando
			$args['importar'] = true;
			$uploadOk         = 1;

			// Detectar la extensión del archivo
			$fileType = strtolower(pathinfo(basename($_FILES['import_file']['name']), PATHINFO_EXTENSION));

			// Solo permitir archivos de extensión csv
			if (!in_array($fileType, ['csv', 'xls', 'xlsx'])) {
				$args['mensaje']  = __('This file type is not allowed.', 'wp_importer');
				$args['importar'] = false;
				$uploadOk         = 0;
			}
		} elseif (isset($_FILES['import_file']) && $_FILES['import_file']['size'] == 0) {
			// Se envió un archivo pero está vacío
			$args['mensaje']  = __('You must upload a file.', 'wp_importer');
			$args['importar'] = false;
		}

		// Si las pruebas sobre el archivo fueron positivas, aplicar la importación
		if ($args['importar']) {
			// Datos para usar la plantilla
			$tpl = 'importers/importer.twig';

			// Tipos
			$args['tipos'] = $tipos;

			// Acciones p2p
			$p2p_action = (isset($_POST['p2p_action'])) ? $_POST['p2p_action'] : StructureImporter::WITHOUT_P2P;

			// Guardar archivio temporal
			$tempfile = wp_tempnam('importer_' . $tipos);
			move_uploaded_file($_FILES['import_file']['tmp_name'], $tempfile);

			// Tipo de archivo
			$fileType = strtolower(pathinfo(basename($_FILES['import_file']['name']), PATHINFO_EXTENSION));

			// Enlace para otro lote
			$args['enlace_otro_lote'] = admin_url('admin.php?import=' . $accion);

			$args['p2p_action'] = $p2p_action;
			$args['filename']     = $tempfile;
			$args['filetype']     = $fileType;
		}

		// Datos para el archivo de referencia que estará disponible para que los usuarios lo descarguen.
		$args['enlace_exportar'] = admin_url('admin-ajax.php?action=export_' . $tipos);

		// Spinner
		$args['spinner'] = $url . 'assets/images/spinner.svg';

		// Textos
		$args['texto'] = $textos;

		// Textos
		$args['action'] = $accion;

		// Twig
		return [$tpl, $args];
	}
}
