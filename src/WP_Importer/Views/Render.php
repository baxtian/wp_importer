<?php

namespace Baxtian\WP_Importer\Views;

use Timber\Timber;

/**
 * Clase base para exportar archivo CSV
 */
class Render
{
	/**
	 * Función para renderizar una plantilla.
	 * Requiere tener activo el plugin Timber.
	 * Lea la documentación de Timber en http://timber.github.io/timber/
	 * Timber está basado en Twig.
	 * Lee la documentación de Twig en https://twig.sensiolabs.org/doc/2.x/
	 * @param  string  $tpl   Plantilla twig a ser usada
	 * @param  array   $args  Arregloc on los argumentos a ser enviados a la plantilla
	 * @param  bool $echo  Si desea capturar la salida del render use el parámetro echo. Si es false devolverá
	 * 						  la información directamente en vez de imprimirla. Por defecto es true.
	 * @return boolean|string Si echo es true indica si pudo o no renderizar la plantilla. Si es false retornará la
	 * 						  cadena con el texto renderizado o false si no pudo ejecutar la acción
	 * @codeCoverageIgnore
	 */
	public function render(string $tpl, array $args, bool $echo = true)
	{
		// Obtener contexto para timber
		$context = Timber::context();

		$filename = dirname(__DIR__, 3) . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $tpl; 

		// Render
		if ($echo) {
			return Timber::render($filename, array_merge($context, $args));
		} 
		
		// ¿o devolver la cadena?
		return Timber::compile($filename, array_merge($context, $args));
	}
}
